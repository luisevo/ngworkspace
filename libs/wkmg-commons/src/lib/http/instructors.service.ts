import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../apps/wkmg/src/environments/environment';
import {Observable} from 'rxjs';
import {InstructorModel, InstructorResponse} from '@wkmg/models';
import {map} from 'rxjs/operators';
// import {SessionService} from '@wkmg/commons';

@Injectable()
export class InstructorsService {
  endpoind = `${environment.API_URL}/instructors`;

  constructor(
    private http: HttpClient,
    // private session: SessionService,
  ) {
  }

  getAll(): Observable<InstructorModel[]> {
    return this.http.get<InstructorResponse[]>(`${this.endpoind}`)
      .pipe(
        map((res: InstructorResponse[]) => {
          return res.map( item => new InstructorModel(item));
        })
      );
  }

  create(body: object): Observable<InstructorModel> {
    /*
    const headers = {
      Authorization: `Bearer ${this.session.token}`
    };
    */
    return this.http.post<InstructorResponse>(`${this.endpoind}`, body)
      .pipe(
        map((res: InstructorResponse) => {
          return new InstructorModel(res);
        })
      );
  }

  update(id: string, body: object): Observable<InstructorModel> {
    return this.http.put<InstructorResponse>(`${this.endpoind}/${id}`, body)
      .pipe(
        map((res: InstructorResponse) => {
          return new InstructorModel(res);
        })
      );
  }

  delete(id: string): Observable<{ _id: string }> {
    return this.http.delete<{ _id: string }>(`${this.endpoind}/${id}`);
  }

}
