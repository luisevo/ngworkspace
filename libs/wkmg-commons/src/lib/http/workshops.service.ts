import { Injectable } from '@angular/core';
import {environment} from '../../../../../apps/wkmg/src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {WorkshopModel, WorkshopInterface} from '../model/workshop.model';

@Injectable()
export class WorkshopsService {
  endpoind = `${environment.API_URL}/workshops`;

  constructor(
    private http: HttpClient,
  ) {
  }

  getAll(): Observable<WorkshopModel[]> {
    return this.http.get<WorkshopInterface[]>(`${this.endpoind}`)
      .pipe(
        map((res: WorkshopInterface[]) => {
          return res.map( item => new WorkshopModel(item));
        })
      );
  }

  create(body: object): Observable<WorkshopModel> {
    return this.http.post<WorkshopInterface>(`${this.endpoind}`, body)
      .pipe(
        map((res: WorkshopInterface) => {
          return new WorkshopModel(res);
        })
      );
  }

  update(id: string, body: object): Observable<WorkshopModel> {
    return this.http.put<WorkshopInterface>(`${this.endpoind}/${id}`, body)
      .pipe(
        map((res: WorkshopInterface) => {
          return new WorkshopModel(res);
        })
      );
  }

  delete(id: string): Observable<{ _id: string }> {
    return this.http.delete<{ _id: string }>(`${this.endpoind}/${id}`);
  }

  updateFile(id: string, fileField: string, file: File): Observable<object> {
    const formData = new FormData();
    formData.set(fileField, file);
    return this.http.put(`${this.endpoind}/${fileField}/${id}`, formData);
  }
}
