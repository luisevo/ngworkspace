import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {InstructorsService} from './instructors.service';
import {WorkshopsService} from './workshops.service';

@NgModule({
  imports: [ HttpClientModule ],
  providers: [InstructorsService, WorkshopsService]
})
export class WkmgHttpModule { }
