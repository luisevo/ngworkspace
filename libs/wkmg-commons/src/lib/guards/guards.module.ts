import { NgModule } from '@angular/core';
import {ServicesModule} from '../services/services.module';
import {AuthenticatedGuard} from './authenticated.guard';
import {RolGuard} from './rol.guard';

@NgModule({
  imports: [
    ServicesModule
  ],
  providers: [AuthenticatedGuard, RolGuard]
})
export class GuardsModule { }
