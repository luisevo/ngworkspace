import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, RouterOutlet, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {SessionService} from '@wkmg/commons';

@Injectable()
export class RolGuard implements CanActivate {

  constructor(
    private session: SessionService,
    private router: Router
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const rols = next.data.rols;
    const hasRol = this.session.user.hasRole(rols);
    if (!hasRol) {
      this.router.navigateByUrl('/auth/sign-in');
    }

    return hasRol;
  }

}
