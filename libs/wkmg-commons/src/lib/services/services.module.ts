import { NgModule } from '@angular/core';
import {SessionService} from './session.service';
import {CryptoService} from './crypto.service';

@NgModule({
  providers: [SessionService, CryptoService]
})
export class ServicesModule { }
