import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import {CryptoService} from './crypto.service';
import {UserModel} from '../model/user.model';

@Injectable()
export class SessionService {
  private key = 'pRdVWI20aT';
  private helper = new JwtHelperService();

  constructor(private crypto: CryptoService) { }

  get user(): UserModel {
    return this.token ? new UserModel(this.helper.decodeToken(this.token)) : null;
  }

  get token() {
    try {
      const storedToken = this.crypto.get(localStorage.getItem(this.key));
      this.helper.decodeToken(storedToken); // verifica la estructura del token
      const isExpired = this.helper.isTokenExpired(storedToken);
      return isExpired ? null : storedToken;
    } catch (e) {
      return null;
    }
  }

  create(token: string) {
    localStorage.setItem(this.key, this.crypto.set(token));
  }

  destroy() {
    localStorage.removeItem(this.key);
  }
}
