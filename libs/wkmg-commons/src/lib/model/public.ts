export * from './instructor.model';
export * from './user.model';
export * from './workshop.model';

// import { InstructorModel } from '@wkmg/models/instructor.model'
// import { UserModel } from '@wkmg/models/user.model'
// import { InstructorModel, UserModel } from '@wkmg/models'
