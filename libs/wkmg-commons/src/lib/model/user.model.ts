export enum UserRol {
  admin = 'ADMIN',
  student = 'STUDENT'
}

interface TokenDecoded {
  email: string;
  userId: string;
  userRol: string;
}

export class UserModel {
  email: string;
  userId: string;
  userRol: UserRol;

  constructor(tokenDecoded: TokenDecoded) {
    this.email = tokenDecoded.email || '';
    this.userId = tokenDecoded.userId || '';
    this.userRol = tokenDecoded.userRol as UserRol || null;
  }

  isAdmin() {
    return this.userRol === UserRol.admin;
  }

  isStudent() {
    return this.userRol === UserRol.student;
  }

  hasRole(rols: UserRol[]) {
    return rols.includes(this.userRol);
  }

}
