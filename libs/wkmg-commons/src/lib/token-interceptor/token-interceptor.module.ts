import { NgModule } from '@angular/core';
import {TokenInterceptorService} from './token-interceptor.service';
import {ServicesModule} from '../services/services.module';
import {HTTP_INTERCEPTORS} from '@angular/common/http';

@NgModule({
  imports: [ServicesModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ]
})
export class WkmgTokenInterceptorModule { }
