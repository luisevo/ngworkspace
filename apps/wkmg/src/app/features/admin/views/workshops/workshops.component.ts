import { Component, OnInit } from '@angular/core';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {InstructorModel, WorkshopModel} from '@wkmg/models';
import {WorkshopFormComponent} from '../../commons/components/workshop-form/workshop-form.component';
import {HttpErrorResponse} from '@angular/common/http';
import {InstructorsService, WorkshopsService} from '@wkmg/commons';
import {forkJoin} from 'rxjs';
import {FileFormComponent} from '../../commons/components/file-form/file-form.component';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-workshops',
  templateUrl: './workshops.component.html',
  styleUrls: ['./workshops.component.scss']
})
export class WorkshopsComponent implements OnInit {
  displayedColumns: string[] = ['poster', 'temary', 'date', 'start', 'end', 'name', 'publish', 'actions'];
  instructors: InstructorModel[] = [];
  workshops: WorkshopModel[] = [];
  dataSource: MatTableDataSource<WorkshopModel> = new MatTableDataSource<WorkshopModel>([]);

  constructor(
    private workshopsHttp: WorkshopsService,
    private instructorHttp: InstructorsService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    forkJoin(
      this.instructorHttp.getAll(),
      this.workshopsHttp.getAll()
    ).subscribe(
      res => {
        this.instructors = res[0];
        this.workshops = res[1];
        this.refreshData();
      },
      err => console.log(err)
    );
  }

  openDialog(workshop?: WorkshopModel): void {
    const dialogRef = this.dialog.open(WorkshopFormComponent, {
      panelClass: 'modal-lg',
      data: {
        instructors: this.instructors,
        workshop
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        (result.id) ? this.update(result) : this.create(result);
      }
    });
  }

  openFileForm(id: string, fileField: string, validExtensions: string[]): void {
    const dialogRef = this.dialog.open(FileFormComponent, {
      width: '350px',
      data: {validExtensions}
    });

    dialogRef.afterClosed().subscribe((file: File) => {
      if (file) { this.updateFile(id, fileField, file); }
    });
  }

  delete(id: string) {
    this.workshopsHttp.delete(id)
      .subscribe(
        res => {
          const index = this.workshops.findIndex(instructor => instructor.id === id);
          this.workshops.splice(index, 1);
          this.refreshData();
        }, (err: HttpErrorResponse) => console.log(err.status)
      );
  }

  update(formData) {
    const { id, date, start, end, description, name, instructor, poster, temary, publish } = formData;
    this.workshopsHttp.update(id, {id, date, start, end, description, name, instructor, poster, temary, publish })
      .subscribe(
        res => {
          const index = this.workshops.findIndex(workshop => workshop.id === id);
          this.workshops[index] = res;
          this.refreshData();
        }, err => console.log(err)
      );
  }

  updateFile(id: string, fileField: string, file: File) {
    this.workshopsHttp.updateFile(id, fileField, file)
      .subscribe(
        res => {
          const url = `${environment.API_URL }/${res[fileField]}`;
          const index = this.workshops.findIndex(item => item.id === id);
          this.workshops[index][fileField] = url;
          this.refreshData();
        }, err => console.log(err)
      );
  }

  create(formData) {
    this.workshopsHttp.create(formData)
      .subscribe(
        res => {
          this.workshops.push(res);
          this.refreshData();
        }, err => console.log(err)
      );
  }

  refreshData() {
    this.dataSource = new MatTableDataSource<WorkshopModel>(this.workshops);
  }
}
