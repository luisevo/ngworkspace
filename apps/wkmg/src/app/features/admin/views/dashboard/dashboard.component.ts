import { Component, OnInit } from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {Label} from 'ng2-charts';
import {WorkshopsService} from '@wkmg/commons';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;

  public barChartData: ChartDataSets[] = [
  ];

  constructor(private workshopsHttp: WorkshopsService) {}

  ngOnInit() {
    this.workshopsHttp.getAll()
      .subscribe(
        workshops => {

          const chartsData = {};
          workshops.forEach((workshop, index) => {
            if (!(workshop.instructor.id in chartsData)) {
              const data = Array.from(new Array(workshops.length).keys()).map(key => 0);
              chartsData[workshop.instructor.id] = { data: [...data], label: workshop.instructor.lastNames };
            }

            chartsData[workshop.instructor.id].data[index] = workshop.participants;
          });

          this.barChartLabels = workshops.map(workshop => workshop.name);
          this.barChartData = Object.values(chartsData);

        }, err => console.log(err)
      );
  }

}
