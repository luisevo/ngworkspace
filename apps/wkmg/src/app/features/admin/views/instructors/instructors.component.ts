import { Component, OnInit } from '@angular/core';
import {InstructorModel} from '@wkmg/models';
import {InstructorsService} from '@wkmg/commons';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {InstructorFormComponent} from '../../commons/components/instructor-form/instructor-form.component';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-instructors',
  templateUrl: './instructors.component.html',
  styleUrls: ['./instructors.component.scss']
})
export class InstructorsComponent implements OnInit {
  displayedColumns: string[] = ['title', 'fullName', 'mail', 'actions'];
  instructors: InstructorModel[] = [];
  dataSource: MatTableDataSource<InstructorModel> = new MatTableDataSource<InstructorModel>([]);

  constructor(
    private instructorHttp: InstructorsService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.instructorHttp.getAll()
      .subscribe(
        res => {
          this.instructors = res;
          this.refreshData();
        },
        err => console.log(err)
      );
  }

  openDialog(instructor?: InstructorModel): void {
    const dialogRef = this.dialog.open(InstructorFormComponent, {
      width: '350px',
      data: {instructor}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        (result.id) ? this.update(result) : this.create(result);
      }
    });
  }

  delete(id: string) {
    this.instructorHttp.delete(id)
      .subscribe(
        res => {
          const index = this.instructors.findIndex(instructor => instructor.id === id);
          this.instructors.splice(index, 1);
          this.refreshData();
        }, (err: HttpErrorResponse) => console.log(err.status)
      );
  }

  update(formData) {
    const { id, title, names, lastNames, mail } = formData;
    this.instructorHttp.update(id, {title, names, lastNames, mail})
      .subscribe(
        res => {
          const index = this.instructors.findIndex(instructor => instructor.id === id);
          this.instructors[index] = res;
          this.refreshData();
        }, err => console.log(err)
      );
  }

  create(formData) {
    this.instructorHttp.create(formData)
      .subscribe(
        res => {
          this.instructors.push(res);
          this.refreshData();
        }, err => console.log(err)
      );
  }

  refreshData() {
    this.dataSource = new MatTableDataSource<InstructorModel>(this.instructors);
  }

}
