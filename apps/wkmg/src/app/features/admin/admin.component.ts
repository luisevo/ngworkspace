import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  menuOptions = [
    { icon: 'home', title: 'Inicio', path: 'dashboard' },
    { icon: 'home', title: 'Instructores', path: 'instructors' },
    { icon: 'home', title: 'Talleres', path: 'workshops' },
  ];

  constructor() { }

  ngOnInit() {
  }

}
