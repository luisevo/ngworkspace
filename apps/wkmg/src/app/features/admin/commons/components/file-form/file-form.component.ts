import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {validExtension} from '@wkmg/commons';

@Component({
  selector: 'app-file-form',
  templateUrl: './file-form.component.html',
  styleUrls: ['./file-form.component.scss']
})
export class FileFormComponent implements OnInit {
  form: FormGroup;

  get fileField() {
    return this.form.get('file');
  }

  constructor(
    public dialogRef: MatDialogRef<FileFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { validExtensions?: string[] },
    private fb: FormBuilder,
  ) {
    this.form = this.fb.group({
      file: ['', validExtension(this.data.validExtensions)]
    });
  }

  ngOnInit() {
  }

  onFileChange(event) {
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.form.patchValue({
        file
      });
    }
  }

  save() {
    this.dialogRef.close(this.fileField.value);
  }

}
