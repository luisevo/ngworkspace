import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { InstructorFormComponent } from './instructor-form/instructor-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { WorkshopFormComponent } from './workshop-form/workshop-form.component';
import { FileFormComponent } from './file-form/file-form.component';

@NgModule({
  entryComponents: [
    InstructorFormComponent,
    WorkshopFormComponent,
    FileFormComponent
  ],
  declarations: [
    InstructorFormComponent,
    WorkshopFormComponent,
    FileFormComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule
  ],
})
export class ComponentsModule { }
