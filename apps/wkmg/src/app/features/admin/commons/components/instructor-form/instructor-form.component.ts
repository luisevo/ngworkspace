import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {InstructorModel} from '@wkmg/models';

@Component({
  selector: 'app-instructor-form',
  templateUrl: './instructor-form.component.html',
  styleUrls: ['./instructor-form.component.scss']
})
export class InstructorFormComponent implements OnInit {
  form: FormGroup;

  get titleField() {
    return this.form.get('title');
  }

  get namesField() {
    return this.form.get('names');
  }

  get lastNamesField() {
    return this.form.get('lastNames');
  }

  get mailField() {
    return this.form.get('mail');
  }

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<InstructorFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { instructor?: InstructorModel }
  ) {
    this.form = this.fb.group({
      id: null,
      title: ['', Validators.required],
      names: ['', Validators.required],
      lastNames: ['', Validators.required],
      mail: ['', [Validators.required, Validators.email]],
    });
  }

  ngOnInit() {
    if (this.data.instructor) {
      const { id, title, names, lastNames, mail } = this.data.instructor;
      this.form.patchValue({
        id, title, names, lastNames, mail
      });
    }
  }

  save() {
    if (this.form.valid) {
      this.dialogRef.close(this.form.value);
    }
  }

  close() {
    this.dialogRef.close();
  }

}
