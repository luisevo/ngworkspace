import { NgModule } from '@angular/core';
import {MaterialModule} from './material/material.module';
import {ComponentsModule} from './components/components.module';
// Sirve para habilitar directivas estructurales (*ngIf, *ngFor, etc)
// import { CommonModule } from '@angular/common';

const MODULES = [
  MaterialModule,
  ComponentsModule
];

@NgModule({
  declarations: [], // declarar componentes
  imports: [...MODULES], // importar las caracteristicas de un modulo
  exports: [...MODULES], // exportar o publicar un o varios sub modulo o componentes
  providers: [] // declarar clases inyectables (Services, Guards, Interceptors)
})
export class AdminCommonsModule { }
// funcion del commons, importar y exportar submodulos
