import { NgModule } from '@angular/core';
import {
  MatButtonModule, MatCheckboxModule, MatDatepickerModule,
  MatDialogModule, MatFormFieldModule,
  MatIconModule, MatInputModule,
  MatListModule, MatNativeDateModule, MatSelectModule,
  MatSidenavModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';

const MODULES = [
  MatSidenavModule,
  MatToolbarModule,
  MatButtonModule,
  MatIconModule,
  MatListModule,
  MatTableModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatCheckboxModule,

  // Datepicker
  MatDatepickerModule,
  MatNativeDateModule,
  //
];

@NgModule({
  imports: [ ...MODULES ],
  exports: [ ...MODULES ]
})
export class MaterialModule { }
