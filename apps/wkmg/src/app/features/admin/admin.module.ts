import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import {ServicesModule, WkmgHttpModule, WkmgPipesModule, WkmgTokenInterceptorModule} from '@wkmg/commons';
import { AdminComponent } from './admin.component';
import {AdminCommonsModule} from './commons/commons.module';
import { InstructorsComponent } from './views/instructors/instructors.component';
import { WorkshopsComponent } from './views/workshops/workshops.component';
import {ChartsModule} from 'ng2-charts';

@NgModule({
  declarations: [DashboardComponent, AdminComponent, InstructorsComponent, WorkshopsComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    AdminCommonsModule,
    ServicesModule,
    WkmgHttpModule,
    WkmgTokenInterceptorModule,
    WkmgPipesModule,
    ChartsModule
  ]
})
export class AdminModule { }
