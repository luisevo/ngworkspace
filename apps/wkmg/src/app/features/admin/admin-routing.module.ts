import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './views/dashboard/dashboard.component';
import {AdminComponent} from './admin.component';
import {InstructorsComponent} from './views/instructors/instructors.component';
import {WorkshopsComponent} from './views/workshops/workshops.component';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard' },
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
      {
        path: 'instructors',
        component: InstructorsComponent
      },
      {
        path: 'workshops',
        component: WorkshopsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
