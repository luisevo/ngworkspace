import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { SignInComponent } from './views/sign-in/sign-in.component';
import {CommonsModule} from './commons/commons.module';
import {ReactiveFormsModule} from '@angular/forms';
import {ServicesModule} from '@wkmg/commons';
import { SignUpComponent } from './views/sign-up/sign-up.component';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [
    SignInComponent,
    SignUpComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    CommonsModule,
    ReactiveFormsModule,
    ServicesModule,
    TranslateModule.forChild()
  ]
})
export class AuthModule { }
