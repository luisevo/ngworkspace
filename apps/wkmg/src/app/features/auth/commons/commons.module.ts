import { NgModule } from '@angular/core';
import {MaterialModule} from './material/material.module';
import {HttpModule} from './http/http.module';

const MODULES = [
  MaterialModule,
  HttpModule
];

@NgModule({
  imports: [...MODULES],
  exports: [...MODULES],
})
export class CommonsModule { }
