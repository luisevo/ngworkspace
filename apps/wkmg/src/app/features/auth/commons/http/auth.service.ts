import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class AuthService {
  endpoint = `${environment.API_URL}/auth`;

  constructor(private http: HttpClient) { }

  signIn(body: {email: string; password: string}): Observable<{token: string}> {
    return this.http.post<{token: string}>(`${this.endpoint}/sign-in`, body);
  }

  signUp(body: {email: string; password: string}): Observable<object> {
    return this.http.post(`${this.endpoint}/sign-up`, body);
  }
}
