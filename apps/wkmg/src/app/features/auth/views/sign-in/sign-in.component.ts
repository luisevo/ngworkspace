import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../commons/http/auth.service';
import {SessionService} from '@wkmg/commons';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  authForm: FormGroup;
  showPassword: boolean;

  get emailField() {
    return this.authForm.get('email');
  }

  get passwordField() {
    return this.authForm.get('password');
  }

  constructor(
    private fb: FormBuilder,
    private authHttp: AuthService,
    private session: SessionService,
    private router: Router
  ) {
    this.authForm = this.fb.group({
      email: ['admin@galaxy.edu.pe', [Validators.required, Validators.email]],
      password: ['123456', Validators.required]
    });
  }

  signIn() {
    this.authHttp.signIn(this.authForm.value)
      .subscribe(
        res => {
          this.session.create(res.token);
          this.router.navigateByUrl('/admin');
        },
        err => console.log(err)
      );
  }

  ngOnInit() {
  }

}
