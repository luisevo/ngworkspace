import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../commons/http/auth.service';
import {passwordStrong, SessionService} from '@wkmg/commons';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  authForm: FormGroup;
  showPassword: boolean;

  get emailField() {
    return this.authForm.get('email');
  }

  get passwordField() {
    return this.authForm.get('password');
  }

  constructor(
    private fb: FormBuilder,
    private authHttp: AuthService,
    private session: SessionService,
    private router: Router
  ) {
    this.authForm = this.fb.group({
      names: ['', Validators.required],
      lastNames: ['', Validators.required],
      documentNumber: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, passwordStrong]]
    });
  }

  signUp() {
    this.authHttp.signUp(this.authForm.value)
      .subscribe(
        res => {
          console.log(res);
          // this.router.navigateByUrl('/admin');
        },
        err => console.log(err)
      );
  }

  ngOnInit() {
  }

}
